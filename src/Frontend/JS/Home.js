const max = 100;

function update() {
    superagent.get('/api/status').end( (err, res) => {
        if (err) {
            console.log(err);
            return;
        }
        const data = JSON.parse(res.text);

        const cont = document.getElementsByClassName('outercontainer')[0];
        const arr = [];
        for (const project of data) {
            const aContainer = document.createElement('h2');
            aContainer.innerText = project.name;
            cont.appendChild(aContainer);
            const container = document.createElement('div');
            container.className = 'container';
            const nDiv = document.createElement('div');
            nDiv.className = 'status txt';
            const img = new Image(max, max);
            img.src = project.status ?  '/Assets/Green.png' : '/Assets/Red.png';
            img.className = 'imgStatus';
            nDiv.innerHTML = `${img.outerHTML}<br><br>Status: ${project.status ? `Online<br>Latency: ${project.latency}` : 'Offline'}`;
            if (project.url) {
                nDiv.innerHTML += `<br>URL: `;
                const link = document.createElement('a');
                link.href = project.url;
                link.innerText = project.url;
                nDiv.innerHTML += link.outerHTML;
            }
            container.appendChild(nDiv);
            if (project.api) {
                const nADiv = document.createElement('div');
                nADiv.className = 'status txt';
                const img = new Image(max, max);
                img.src = project.status ?  '/Assets/Green_API.png' : '/Assets/Red_API.png';
                img.className = 'imgStatus';
                nADiv.innerHTML = `${img.outerHTML}<br><br>Status: ${project.status ? `Online<br>Latency: ${project.latency}` : 'Offline'}`;
                container.appendChild(nADiv);
            }
            arr.push(container);
        }
        cont.innerHTML = '';
        for (const project of arr) {
            cont.appendChild(project);
        }
    } );
}

function load() {
    const secs = 30000;
    superagent.get('/api/status').end( (err, res) => {
        if (err) {
            console.log(err);
            const notice = document.getElementById('noticetxt');
            notice.innerHTML = 'Something unknown happened.';
            setInterval( () => update(), secs);
            return;
        }
        const data = JSON.parse(res.text);

        const cont = document.getElementsByClassName('outercontainer')[0];
        for (const project of data) {
            const aContainer = document.createElement('h2');
            aContainer.innerText = project.name;
            cont.appendChild(aContainer);
            const container = document.createElement('div');
            container.className = 'container';
            const nDiv = document.createElement('div');
            nDiv.className = 'status txt';
            const img = new Image(max, max);
            img.src = project.status ?  '/Assets/Green.png' : '/Assets/Red.png';
            img.className = 'imgStatus';
            nDiv.innerHTML = `${img.outerHTML}<br><br>Status: ${project.status ? `Online<br>Latency: ${project.latency}` : 'Offline'}`;
            if (project.url) {
                nDiv.innerHTML += `<br>URL: `;
                const link = document.createElement('a');
                link.href = project.url;
                link.innerText = project.url;
                nDiv.innerHTML += link.outerHTML;
            }
            container.appendChild(nDiv);
            if (project.api) {
                const nADiv = document.createElement('div');
                nADiv.className = 'status txt';
                const img = new Image(max, max);
                img.src = project.status ?  '/Assets/Green_API.png' : '/Assets/Red_API.png';
                img.className = 'imgStatus';
                nADiv.innerHTML = `${img.outerHTML}<br><br>Status: ${project.status ? `Online<br>Latency: ${project.latency}` : 'Offline'}`;
                container.appendChild(nADiv);
            }
            cont.appendChild(container);
        }
    } );
    setInterval( () => update(), secs);
}
