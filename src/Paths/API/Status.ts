import Path from '../../Structures/Path';
import Base from '../../Structures/Base';
import { Response } from 'express';

class Status extends Path {
    constructor(base: Base) {
        super(base);
        this.label = '[API] Status';
        this.path = '/api/status';
    }

    execute(req: any, res: any): Promise<Response | void> {
        if (!this.base.ready || !this.base.projectStatus) {
            return Promise.resolve(res.status(this.codes.internalErr).send('[INTERNAL ERROR] Status Master not ready!') );
        }
        return Promise.resolve(res.status(this.codes.ok).send(this.base.projectStatus) );
    }
}

export default Status;
