import express, {Response, Request} from 'express';
import https from 'https';
import Events from 'events';
import superagent, { SuperAgentStatic } from 'superagent';
import { promisify } from 'util';
import { join } from 'path';
import { readFileSync } from "fs";

import Projects, { Project } from './Projects';
import Config from './Config';
import config from '../../config.json';
import Path from './Path';
import * as Paths from '../Paths/index.js';
const sleep = promisify(setTimeout);

const ee = new Events();

ee.on('fail', async() => {
    const fivesecs = 5000;
    await sleep(fivesecs);
    process.exit();
} );

interface Status {
    status: boolean;
    latency?: string;
    api?: {
        latency?: string;
        status: boolean;
    };
    url?: string;
    name: string;
}

export default class Base {
    public web: express.Application;

    public projects?: Project[];

    public config: Config;

    public superagent: SuperAgentStatic;

    public paths: Map<string, object>;

    public ips: Map<string, number>;

    public ipBans: string[];

    public projectStatus?: Status[];

    public ready: boolean;

    public projectsReady: boolean;

    private _projects: Projects;

    constructor() {
        this._projects = new Projects();
        this.projectsReady = false;
        this._projects.on('ready', () => {
            this.projectsReady = true;
        } );
        this._projects.verifyProjects();
        this.web = express();
        this.config = new Config(config);
        this.superagent = superagent;
        this.paths = new Map();
        this.ips = new Map();
        this.ipBans = [];
        this.projectStatus = [];
        this.ready = false;
        this.handleHeaders = this.handleHeaders.bind(this);
    }

    /**
     * Handle setting headers
     *
     * @param {Object<Request>} req The express request
     * @param {Object<Response>} res The express response
     * @param next
     */
    handleHeaders(req: Request, res: Response, next: any): any {
        if (!this.config.certOptions || !this.config.certOptions.cert || !this.config.certOptions.key) {
            res.set( {
                'Content-Security-Policy': 'frame-ancestors \'none\'',
                'X-Frame-Options': 'DENY',
                'Referrer-Policy': 'no-referrer, origin-when-cross-origin',
                'X-XSS-Protection': '1; mode=block',
                'X-Content-Type-Options': 'nosniff',
            } );
        } else {
            res.set( {
                'Content-Security-Policy': 'frame-ancestors \'none\'',
                'X-Frame-Options': 'DENY',
                'Referrer-Policy': 'no-referrer, origin-when-cross-origin',
                'X-XSS-Protection': '1; mode=block',
                'X-Content-Type-Options': 'nosniff',
                'Strict-Transport-Security': 'max-age=63072000; includeSubDomains; preload',
            } );
        }
        next();
    }


    /**
     * Initialize a path
     *
     * @param {Object<Path>} path The path to initialize
     * @private
     */
    _initPath(path: Path): void {
        // Handle if the path is a bad path
        if (!path.label || !path.path) {
            throw Error(`[ERROR] Path ${path.path || path.label} label and or path not found!`);
        }
        if (!path.execute) {
            throw Error(`[ERROR] Path ${path.label} does not have an execute method!`);
        }
        // Set the path, then initiate the path on the web server. I will probably set up a better method later
        this.paths.set(path.label, path);

        // Init the path with the web app
        if (path.type === 'post') {
            this.web.post(path.path, (req, res) => path._execute(req, res) );
        } else if (path.type === 'delete') {
            this.web.delete(path.path, (req, res) => path._execute(req, res) );
        } else if (path.type === 'patch') {
            this.web.patch(path.path, (req, res) => path._execute(req, res) );
        } else {
            this.web.get(path.path, (req, res) => path._execute(req, res) );
        }
    }

    initPaths(): void {
        let pathNums = 0;
        for (const path in Paths) {
            const mName: string = path;
            // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
            // @ts-ignore
            const Ok = Paths[path];
            const apath: Path = new Ok(this);
            if (apath.enabled) { // If the path should be loaded
                console.log(`[SYSTEM INIT PATH] - Initializing Path ${apath.label}`);
                // Init the path
                this._initPath(apath);
                // Tell the user the path was initialized and add the number of paths loaded by 1
                console.log(`[SYSTEM INIT PATH] - Initialized path ${apath.label} (${mName}) with type ${apath.type}!`);
                pathNums++;
            }
        }
        console.log(`[SYSTEM INIT] Initialized ${pathNums} paths`);
    }

    async checkProjects(): Promise<Project[]> {
        if (!this.projectsReady) {
            const secs = 1000;
            setTimeout( () => this.checkProjects(), secs);
        }
        this.projects = this._projects.projects;
        this.ready = true;
        return this.projects;
    }

    async init(): Promise<void> {
        let uhm;

        try {
            uhm = await this.superagent.get(`localhost:${this.config.port}`);
        } catch (err) {
            if (err.message.startsWith('connect ECONNREFUSED') ) {
                // I dont care about this error
            } else {
                throw Error(err);
            }
        }
        // If a user is trying to listen to a port already used
        if (uhm) {
            ee.emit('fail');
            throw Error('[FATAL] You are trying to listen on a port in use!');
        }
        this.web.all('*', this.handleHeaders);
        this.initPaths();
        await this.checkProjects();
        if (!this.config.certOptions || !this.config.certOptions.key || !this.config.certOptions.cert) {
            this.web.listen(config.port);
        } else {
            this.config.certOptions.key = readFileSync(this.config.certOptions.key);
            this.config.certOptions.cert = readFileSync(this.config.certOptions.cert);
            if (this.config.certOptions.ca && Array.isArray(this.config.certOptions.ca) ) {
                const cas = [];
                for (const ca of this.config.certOptions.ca) {
                    cas.push(readFileSync(ca) );
                }
                this.config.certOptions.ca = cas;
            }

            const httpOptions = this.config.certOptions;
            const server = https.createServer(httpOptions, this.web);
            server.listen(this.config.port);
        }
        this.web.use(express.static(join(__dirname, '../Frontend') ) );
        this.web.use('/Assets/', express.static(join(__dirname, '../Assets') ) );
        this.checkStatuses();
        console.log('[SYSTEM INFO] Initialized!');
        const twomin = 120000;
        setInterval( () => this.checkStatuses(), twomin);
    }

    async checkStatuses(): Promise<void> {
        if (!this.ready || !this.projects) {
            return;
        }
        const arr = [];
        for (const project of this.projects) {
            const obj: Status = {
                status: false,
                name: project.name,
            };
            try {
                const time = new Date();
                await this.superagent.get(project.url);
                // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
                // @ts-ignore
                obj.latency = `${new Date() - time}ms`;
                obj.status = true;
            } catch (err) {
                obj.status = false;
            }
            if (project.api) {
                obj.api = {
                    status: false,
                };
                try {
                    const time = new Date();
                    await this.superagent.get(project.api);
                    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
                    // @ts-ignore
                    obj.api.latency = `${new Date() - time}ms`;
                    obj.api.status = true;
                } catch (err) {
                    obj.api.status = false;
                }
            }
            if (project.showUrl) {
                obj.url = project.url;
            }
            arr.push(obj);
        }
        this.projectStatus = arr;
    }
}
