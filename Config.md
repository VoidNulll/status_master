# Status Master Config

## Keys

example layout | type of key | description (| notice?)

port | number | The port Status master will listen to.

certOptions | object | If you have a certificate, set it here.

certOptions.cert | string | The path to your certificate.

certOptions.key | string | The path to your certificate key.

certOptions.requestCert | string??? | ???? (See nodejs TLS documentation)

certOptions.ca | string[] | ??? (See nodejs TLS documentation)

# Default Config

```json
{
    "port": 8888
}
```
