# Status Master Projects

# Keys

url | string | The url to check for

name | string | The name to call this

api? | string | The URL to the API | optional

showUrl | boolean | Whether or not to show the URL on the status page. | optional

# Example Projects

```json
{
    "url": "https://example.com",
    "name": "Example"
}
```

```json
{
    "url": "https://example.com",
    "name": "Example",
    "api": "https://example.com/api"
}
```

```json
{
    "url": "https://example.com",
    "name": "Example",
    "api": "https://example.com/api",
    "showUrl": true
}
```

# Layout example

```json
[
    {
        "url": "https://example.com",
        "name": "Example"
    }
]
```
